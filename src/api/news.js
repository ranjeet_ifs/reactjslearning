export const getNewsArticle = async (category) => { 
    const API_KEY = '55f9d08df136495ea8a5703e71b88a00';
    const API_URL = `https://newsapi.org/v2/top-headlines?country=in&pageSize=100&category=${category}&apiKey=${API_KEY}`
    try {
        let response = await fetch(API_URL);
        return await response.json();
    } catch (err) { 
        console.log(`Error has occured`, err)
    }
}