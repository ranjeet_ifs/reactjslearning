import { call, put, takeEvery } from 'redux-saga/effects';
import * as actionTypes from '../actions/actionTypes';
import { getNewsArticle } from '../api/news';

function* fetchArticles(action) { 
    try {
        const news = yield call(getNewsArticle, action.payload);
        yield put({ type: actionTypes.GET_NEWS_ARTICLES_SUCCESS, data: news });
    } catch (e) { 
        yield put({ type: actionTypes.GET_NEWS_ARTICLES_FAILED, error: true });
    }
}

function* newsSaga() { 
    yield takeEvery(actionTypes.GET_NEWS_ARTICLES_REQUESTED, fetchArticles);
}

export default newsSaga;
