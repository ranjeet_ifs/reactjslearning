import React from 'react';

import NewsFeeds from './components/NewsFeeds';

const App = () => {

  return (
    <>
      <NewsFeeds/>
    </>
  );
}

export default App;