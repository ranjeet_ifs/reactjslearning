import * as actionTypes from './actionTypes';

export function getNewsArticles(category) { 
    return {
        type: actionTypes.GET_NEWS_ARTICLES_REQUESTED,
        payload:category
    }
}