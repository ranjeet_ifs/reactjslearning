import { combineReducers } from "redux";
import news from './newArticleReducers';

export default combineReducers({
    news,
});