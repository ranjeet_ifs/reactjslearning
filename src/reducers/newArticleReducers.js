import * as actionTypes from '../actions/actionTypes';

const initialState = {
    data: [],
    loading: false,
    error:null
}

const newArticleReducer = (state = initialState, action) => { 
    switch (action.type) { 
        case actionTypes.GET_NEWS_ARTICLES_REQUESTED:
            return {
                ...state,
                loading:true,
            }
        case actionTypes.GET_NEWS_ARTICLES_SUCCESS: { 
            return {
                ...state,
                data: action.data,
                loading:false
            }
        }
        case actionTypes.GET_NEWS_ARTICLES_FAILED:
            return {
                ...state,
                loading: false,
                error:action.error
            }
        default:
            return state;
    }
}

export default newArticleReducer