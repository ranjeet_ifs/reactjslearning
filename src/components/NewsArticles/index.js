import React from 'react';
import NoImage from '../../assets/no-image.jpg';

const NewsArticles = ({article, itemid}) => {
    return (  
          <div className="flex w-full sm:w-1/2 md:w-1/2 xl:w-1/4 p-3 items-stretch" key={itemid}>
            <a href={article.url} target="_blank" rel="noreferrer" className="c-card block bg-white dark:bg-zinc-900 shadow-md hover:shadow-xl rounded-lg overflow-hidden">
              <div className="relative pb-48 overflow-hidden">
                {article.urlToImage !== null ?  <img className="absolute inset-0 h-full w-full object-cover" src={article.urlToImage} alt={article.title}/> :  <img className="absolute inset-0 h-full w-full object-cover" src={NoImage} alt={article.title}/>}
              </div>
              <div className="p-4">
                <span className="inline-block px-2 py-1 leading-none bg-zinc-800 text-white rounded-full font-semibold uppercase tracking-wide text-xs">{article.source.name}</span>
                  <h2 className="mt-2 mb-2 font-bold line-clamp-2 dark:text-white">{article.title}</h2>
                  <p className="text-sm line-clamp-2 dark:text-white">{article.description ? article.description : article.content}</p>
                <div className="mt-3 flex items-center">
                  <button type='buttom' href={article.url}  className="inline-block px-3 py-1 border border-red-500 text-red-500 font-medium text-xs leading-tight uppercase rounded-md hover:bg-red-600 hover:text-white focus:outline-none focus:ring-0 transition duration-150 ease-in-out">Read More</button>
                </div>
              </div>
            </a>
          </div>  
    );
}

export default NewsArticles;