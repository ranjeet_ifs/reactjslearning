import React from 'react';
//import DropDown from '../DropDown';
import Switcher from '../Switcher';
import CategoryMenu from '../CategoryMenu';
import DateTime from '../DateTime';
import TextBox from '../TextBox';

const Header = ({inputValue, onChangeInput}) => {
    return (
        <>
        <div className="flex bg-red-500 sticky top-0 z-10 shadow-md">
            <div className='container mx-auto pb-3 pt-3 overflow-x-hidden pl-4 pr-4 lg:pl-0 lg:pr-0'>
                <div className='flex justify-between'>
                    <h3 className='text-white font-bold text-md lg:text-xl'>NEWS FEEDS</h3>
                    <div className='w-96 hidden lg:block'><TextBox inputValue={inputValue} onChangeInput={onChangeInput} /></div>
                    <div className="flex justify-center items-center">
                    <div className='pr-4'><Switcher /></div>
                    <div className='relative right-0 w-40'>
                    <DateTime/>
                    </div>
                    {/* <DropDown newCategoryHandler={newCategoryHandler}/> */}
                </div>
               </div>
            </div>
        </div>
        <CategoryMenu />
        </>
    );
}

export default Header;