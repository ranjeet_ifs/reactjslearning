import React, { useEffect, useState } from 'react';
import NewsArticles from '../NewsArticles';
import Header from '../Header';
import Loader from '../Loader';
import { NEWS_CATEGORY } from '../../utils/category';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions';
import ScrollToTop from '../ScrollToTop';
import { useLocation } from "react-router-dom"
import NoResultFound from '../NoResultFound';

const NewsFeeds = () => {
  const dispatch = useDispatch();
  const articles = useSelector((state) => state.news.data.articles);
  const isError = useSelector((state) => state.news.error);
  const isLoading = useSelector((state) => state.news.loading);
  const [inputValue, setInputValue] = useState('');
  const location = useLocation();
  const category = location.pathname.split("/")[1];

  const newCategoryHandler = (e) => {
    console.log(e.target.value);
  }

  const inputValueHandler = (value) => { 
    setInputValue(value);
  }

  useEffect(() => {
    setInputValue('');
    dispatch(actionCreator.getNewsArticles(category ? category : NEWS_CATEGORY[2]));
  }, [category, dispatch]);


  
  return (
    <>
      <main>
        <Header
          newCategoryHandler={newCategoryHandler}
          inputValue={inputValue}
          onChangeInput={inputValueHandler} />
        <div className='wrapper min-h-screen dark:bg-black pt-5'>
        <div className='container mx-auto overflow-x-hidden pl-4 pr-4 lg:pl-2 lg:pr-2'>
        <div className='flex flex-wrap -mx-4'>
          {/* {isLoading ? <Loader /> :
            <>
                {isError && <h3>Something went wrong, please try after sometimes.</h3>}
                   {articles && articles.length && articles.length > 0 ? articles.map((item) => (
                    <NewsArticles article={item} itemid={item.id} key={item.id}/>
                )) : <NoResultFound/>}
            </>
            } */}
              {isLoading && <Loader />}
              {isError && <h3>Something went wrong, please try after sometimes.</h3>}
              {articles && articles.length > 0 ? articles.filter((result) => {
                if (inputValue !== "") {
                  const filteredResult = result.title.toLowerCase().includes(inputValue.toLowerCase())
                  return filteredResult;
                } else { 
                  return result;
                }
              }).map((item) => (
                <NewsArticles article={item} itemid={item.id} key={item.id} />
              ))
                : <NoResultFound/>
              }
          </div>
          </div>
        </div>
        <ScrollToTop/>
      </main>
    </>
  );
}

export default NewsFeeds;