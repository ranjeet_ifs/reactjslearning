import React, { useEffect, useState } from 'react';

const ScrollToTop = () => {
    const thresHold = 200;
    const [showTopBtn, setShowTopBtn] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > thresHold) {
                setShowTopBtn(true);
            } else {
                setShowTopBtn(false);
            }
        });
    }, [])
    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    };
    return (
        <div>
            {showTopBtn && (
                <div onClick={goToTop} className='cursor-pointer'>
                    <div className="fixed bottom-10 lg:right-10 right-5 bg-red-100 dark:bg-red-50 p-2 w-10 h-10 ring-1 ring-slate-900/5 dark:ring-slate-900/20 shadow-xl rounded-full flex items-center justify-center">
                        <svg className="w-6 h-6 text-red-500 rotate-180" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path d="M19 14l-7 7m0 0l-7-7m7 7V3"></path>
                        </svg>
                    </div>
                </div>
            )}
        </div>
    );
}

export default ScrollToTop;