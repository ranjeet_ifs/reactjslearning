import React from 'react';
import {NavLink} from 'react-router-dom';
import { NEWS_CATEGORY } from '../../utils/category';


const CategoryMenu=()=> {
    return (
        <div className='flex bg-white dark:bg-zinc-800 w-full top-12 sticky z-20 border-b dark:border-black'>
            <div className='container mx-auto pb-3 pt-3 overflow-x-hidden pl-4 pr-4 lg:pl-0 lg:pr-0'>
                <div className='flex overflow-auto'>               
                    {NEWS_CATEGORY.map((category) => (
                        <div className="p-1" key={category}>
                            {/* <NavLink to={category} activeClassName='text-red'>{category}</NavLink> */}
                            <NavLink
                                to={category}
                                className={({ isActive }) => 
                                ["group flex items-center px-2 py-1 text-xs font-medium rounded-md uppercase dark:text-white", isActive ? "bg-red-500 text-white" : null,
                                    ].filter(Boolean).join(" ")}>{category}
                            </NavLink>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default CategoryMenu;