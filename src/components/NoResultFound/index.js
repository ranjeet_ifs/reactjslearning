import React from 'react';
import NoResult from '../../assets/no-result.svg';

const NoResultFound=()=> {
    return (
        <div className='container mx-auto pb-3 pt-3 overflow-x-hidden pl-4 pr-4 lg:pl-0 lg:pr-0'>
            <div className='flex justify-center items-center flex-col pt-10'>
            <img src={NoResult} alt="No result found" width={200} height={100}/>
                <h1 className='mt-5 font-bold dark:text-white'>No Result found</h1>
            </div>
        </div>
    );
}

export default NoResultFound;