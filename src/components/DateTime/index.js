import React, { useState, useEffect} from 'react';

const DateTime = () => {
    const [date, setDate] = useState(new Date());
    useEffect(() => { 
        let timer = setInterval(() => setDate(new Date()), 1000);
        return () => { 
            clearInterval(timer);
        }
    })
    return (
        <>
            <span className='text-white text-sm'> {date.toLocaleDateString('en-US',{weekday: 'long'})}, {date.toLocaleTimeString('en-IT')}</span> 
        </>
    );
}

export default DateTime;