import React, { useState } from 'react';
import { DarkModeSwitch } from 'react-toggle-dark-mode';
import useDarkSide from '../../utils/useDarkSide';

const Switcher = () => {
    const [colorTheme, setTheme] = useDarkSide();
    const [darkSide, setDarkSide] = useState(colorTheme === 'light' ? true : false);

    const toggleDarkMode = (checked) => { 
        setTheme(colorTheme);
        setDarkSide(checked);
    }
    
    return (
        <>
            <DarkModeSwitch checked={darkSide} onChange={toggleDarkMode} size={22} />
            {/* <p>{colorTheme === 'light' ? 'Dark mode' : 'Light mode'}</p> */}
        </>
    );
}

export default Switcher;