import React from 'react';

const TextBox = ({inputValue, onChangeInput})=> {
    return (
        <input type="text" value={inputValue} onChange={(e) => {onChangeInput(e.target.value)}} placeholder="Filter by news titles" className="px-3 py-1 placeholder-slate-300 text-slate-600 relative bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full" />
    );
}

export default TextBox;