import React from 'react';
import { NEWS_CATEGORY } from '../../utils/category';

const DropDown = ({newCategoryHandler }) => {
    const categoryHandler = (data) => { 
        newCategoryHandler(data)
    }
    return (
        <>
           <select className="form-select appearance-none
                        inline-block
                        w-30 lg:w-32
                        px-1
                        pl-4
                        pt-1
                        pb-1
                        font-normal
                        text-gray-700
                        border border-solid border-gray-300
                        rounded
                        transition
                        ease-in-out
                        text-sm
                        m-0 focus:text-gray-700 focus:bg-white focus:border-white focus:outline-none" aria-label="Default select example" onChange={(e) => categoryHandler(e)} defaultValue={NEWS_CATEGORY[2]}>
                        {NEWS_CATEGORY.map((category) => (
                            <option value={category} key={category}>{category.toUpperCase()}</option>
                        ))}
                        </select> 
        </>
    );
}

export default DropDown;